const { SlashCommandBuilder } = require('@discordjs/builders');
const { Discord, MessageEmbed } = require('discord.js');
const { name, clientId, permissions} = require(`${process.cwd()}/config.json`)


module.exports = {
	data: new SlashCommandBuilder()
		.setName('server')
		.setDescription('Display info about this server.'),
	async execute(interaction) {
		//return interaction.reply(`Server name: **${interaction.guild.name}**\nTotal members: **${interaction.guild.memberCount}**\nCreated at:**${interaction.guild.createTime}**`);
		function TStoSec(seconds)
		{
			seconds = Math.floor((seconds/1000))
			return seconds;
		}
		interaction.guild.fetch();
		const guildName = interaction.guild.name;
		const guildId = interaction.guild.id;
		const guildIcon = interaction.guild.iconURL('png', 256)
		const guildMem = interaction.guild.memberCount;
		const guildCha = interaction.guild.channels.channelCountWithoutThreads;
		
		if (interaction.guild.available == true) {			
			const userEmbed = new MessageEmbed()
				.setTitle(`${guildName}`)
				.setAuthor({name : `${guildId}`})
				.setDescription(`${interaction.guild.description}`)
				.setThumbnail(guildIcon)
				.setColor(interaction.guild.displayHexColor)
				.addFields(
					{ name: 'Members Joined', value: `${guildMem}`, inline: true },
					{ name: 'Channels', value: `${guildCha}`, inline: true },
					{ name: 'Server created at', value: `<t:${TStoSec(interaction.guild.createdTimestamp)}:f>`, inline: false },			
					)
				//.addField('Inline field title', 'Some value here', true)
				.setTimestamp()
				.setFooter({text: "Nullanoid Development"});

			interaction.reply({ embeds: [userEmbed] });
		}

	},
};
