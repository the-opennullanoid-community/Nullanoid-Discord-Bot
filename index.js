const fs = require('fs');
const { Client, Collection, Intents, Message } = require('discord.js');
const { token, name, activity, status } = require('./config.json');
const AntiSpam = require("discord-anti-spam");
const MessageHandler = require('discord-message-handler').MessageHandler;

console.log(`Initializing ${name} server..`); //Start Initializing Server

const clientIntents = new Intents();
clientIntents.add(Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.GUILD_MESSAGES)
const client = new Client({ intents: clientIntents }); //Instance New Client
console.log(`Setted up client`);

client.commands = new Collection(); // Create a collection of commands
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js')); //Add commands to the collection
//Look for the files of the commands under the ./commands directory and set them
for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.data.name, command);
}
console.log(`Loaded up ${commandFiles.length} commands`);

const handler = new MessageHandler();
const messageHelper = require('./auto-msg/helper');
messageHelper.setup(handler);

const {antiSpam} = require("./auto-msg/anti-spam");
console.log(`Initialized AntiSpam and MessageHandler`);

//Client ready class
client.once('ready', () => {
	console.log(`${name} is now ready to take actions!`);
	console.warn(`Warning! ${name} bot is still under Heavy Development.`)
	//Set the discord status for the bot
	client.user.setActivity(activity, { type: "WATCHING"})
	client.user.setStatus(status);
});

//Allow executing slash commands
client.on('interactionCreate', async interaction => {
	if (!interaction.isCommand()) return; //If its not a command then dont do anything

	const command = client.commands.get(interaction.commandName);

	if (!command) return; //If its not a valid command or a command for a another bot then dont do anything

	// If its actually a command that Nullanoid.js supports and uses then try to execute it
	try {
		await command.execute(interaction); //Executes the command
	} catch (error) {
		console.error(error); //Show the full contents of the error to the log
		return interaction.reply({ content: `There was an error while executing this command! \n${error} `, ephemeral: true }); //Tell the user that there was an error
	}
});



client.on('messageCreate', message => {
	if (message) {
		handler.handleMessage(message);
		antiSpam.message(message);
	}
});


client.login(token); //Log into discord

//Function to handle NodeJS shutdown(Logs Off Discord)
function ServerShutdown(){
	client.user.setActivity("Shutting Down..", { type: "WATCHING"})
	client.user.setStatus("dnd");
	console.warn("\nCaught Shutdown Signal");
	console.log(`${name} is now shutting down..`)
	client.destroy(); // Destroy the client and log off
	process.exit(); // Shut down node.js
}

//Detect NodeJS shutdown Signals
process.on('SIGINT', ServerShutdown)
process.on('SIGTERM', ServerShutdown)
process.on('SIGQUIT', ServerShutdown)
