# Nullanoid - The discord bot
The discord bot for the OpenNullanoid Community discord server. Made with Discord.JS libraries and Node.

Future plans for implementing Anti-Spam and Auto-Mod fetures and fun fetures as well.

### To get started(Using it)
+ Step 1 : Click on the download button before clone to download it in your respective format.
+ Step 2 : Install Node.JS server and install Node Package Manager(NPM) and make sure they are on the correct paths.
+ Step 3 : Then run the `init.sh` for POSIX based OS(Linux, FreeBSD, MacOS-X) or run the `init.bat` for Windows NT(Morden Windows) on a Command-Line session.
+ Step 4 : Doing the step 3 will automatically start the server otherwise run `node .` and shut it down by sending a shut down signal by pressing `CTRL + C`.
