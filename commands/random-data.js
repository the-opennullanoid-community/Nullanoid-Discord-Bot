const { SlashCommandBuilder } = require('@discordjs/builders');
const { Discord, MessageEmbed } = require('discord.js');
const {faker} = require('@faker-js/faker');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('random-data')
		.setDescription(`Generates Advanced random data powered by the Community Maintained Faker.JS`),
	async execute(interaction) {
		const userEmbed = new MessageEmbed()
		.setTitle(`Here is your random information that you've asked for.`)
		.setColor(interaction.member.displayHexColor)
		.addFields(
			{ name: 'Name', value: `${faker.name.findName()}`, inline: false },
			{ name: 'Email Address', value: `${faker.internet.email()}`, inline: false },
			{ name: 'Gender', value: `${faker.name.gender()}`, inline: false },
			{ name: 'Works At', value: `${faker.name.jobTitle()}`, inline: false },
            { name: 'Location', value: `${faker.address.latitude()}`, inline: false },
            { name: 'IP Address', value:`${faker.internet.ipv4()}` ,inline:false},
            { name: 'Phone Number', value: `${faker.phone.phoneNumber()}`, inline: false},	
            { name: 'Website', value: `${faker.internet.domainName()}`, inline: false},
            { name: 'Music Genre', value: `${faker.music.genre()}`, inline: false},
            { name: 'Vehecle', value: `${faker.vehicle.vehicle()}`, inline: false},		
			)
		//.addField('Inline field title', 'Some value here', true)
		.setTimestamp()
		.setFooter({text:"Nullanoid Development"});

		interaction.reply({ embeds: [userEmbed] });
	},
};
