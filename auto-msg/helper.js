const { MessageHandler } = require('discord-message-handler');

module.exports.setup = function(handler) {
    /* [Optional] You can recreate the handler using the parent context so your IDE will properly give out suggestions for the handler */
    const helper = new MessageHandler(handler);
    helper.whenMessageStartsWith("hey").sometimes(50).reply("Yo!");
    helper.whenMessageStartsWith("hi").sometimes(25).reply("**Howdy!** have a nice time!");
    helper.whenMessageContainsExact("how are you nully").reply("You can check my stats by running **/ping** command!");
    helper.whenMessageStartsWith("f").reply("You have paid your respects to the ones who needed it!");
    helper.setCaseSensitive(true);
}